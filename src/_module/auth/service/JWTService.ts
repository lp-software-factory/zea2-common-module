import {Injectable} from '@angular/core';

import {JWTAuthToken} from '../../../../../../definitions/src/zea2/src/definitions/auth/entity/JWTAuthToken';

@Injectable()
export class JWTService
{
	private current: JWTAuthToken;

	public hasJWT(): boolean {
		return this.current !== undefined;
	}

	public getJWT(): string {
		if (this.hasJWT()) {
			return this.current.jwt;
		} else {
			throw new Error('JTW is not available');
		}
	}

	public clearJWT() {
		this.current = undefined;
	}

	public getToken(): JWTAuthToken {
		if (this.hasJWT()) {
			return this.current;
		} else {
			throw new Error('JTW is not available');
		}
	}

	public isAdmin(): boolean {
		return !!~this.current.account.app_access.indexOf('admin');
	}

	public setJWT(token: JWTAuthToken) {
		this.current = token;
	}
}
