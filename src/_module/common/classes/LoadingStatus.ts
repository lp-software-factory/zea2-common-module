import {Injectable} from '@angular/core';

@Injectable()
export class LoadingManager
{
	private loading: LoadingStatus[] = [];

	addLoading(): LoadingStatus {
		let status = {is: true};
		this.loading.push(status);

		return status;
	}

	isLoading(): boolean {
		return this.loading.filter(loading => loading.is === true).length > 0;
	}

	notLoading(): boolean {
		return !this.isLoading();
	}

	clearLoaders() {
		this.loading.map(loading => loading.is = false);
	}
}

export interface LoadingStatus
{
	is: boolean;
}