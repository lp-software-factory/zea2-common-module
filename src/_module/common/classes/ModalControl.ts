import {LoadingManager} from './LoadingStatus';

export class ModalControl<T>
{
	private _status: LoadingManager = new LoadingManager();
	private _opened: boolean = false;
	private _openArgs: T;

	public isOpened() {
		return this._opened;
	}

	get status(): LoadingManager {
		return this._status;
	}

	public open(args: T) {
		this._opened = true;
		this._openArgs = args;
	}

	get openArgs(): T {
		return this._openArgs;
	}

	public close() {
		this._opened = false;
	}
}