export function basename(path, suffix?) {
	let b = path.replace(/^.*[\/\\]/g, '');

	if (typeof(suffix) === 'string' && b.substr(b.length - suffix.length) === suffix) {
		b = b.substr(0, b.length - suffix.length);
	}

	return b;
}
