import {Injectable} from '@angular/core';
import {Http, RequestOptionsArgs, Response} from '@angular/http';

import {JWTService} from "../../auth/service/JWTService";
import {RESTAdapterInterface, RESTAdapterInterfaceOptions, RESTAdapterInterfaceHeader} from "../../../../../../definitions/src/zea2/src/essentials/RESTAdapterInterface";

@Injectable()
export class RESTAdapter implements RESTAdapterInterface
{
	constructor(
		public http: Http,
		public auth: JWTService,
	) {}

	private static prepareRequestOptions(options: RESTAdapterInterfaceOptions): RequestOptionsArgs {
		let result = {};

		result['headers'] = options.headers;
		result['search'] = options.search;

		return result;
	}

	public get(url: string, options: RESTAdapterInterfaceOptions = {}) {
		if (this.auth.hasJWT() && !options.noAuth) {
			options.headers = this.getAuthHeaders();
		}

		return this.handle(this.http.get(url, RESTAdapter.prepareRequestOptions(options)));
	}

	public put(url: string, json: any = '{}', options: RESTAdapterInterfaceOptions = {}) {
		if (this.auth.hasJWT() && !options.noAuth) {
			options.headers = this.getAuthHeaders();
		}

		return this.handle(this.http.put(url, JSON.stringify(json), RESTAdapter.prepareRequestOptions(options)));
	}

	public post(url: string, json: any = '{}', options: RESTAdapterInterfaceOptions = {}) {
		if (this.auth.hasJWT() && !options.noAuth) {
			options.headers = this.getAuthHeaders();
		}

		return this.handle(this.http.post(url, JSON.stringify(json), RESTAdapter.prepareRequestOptions(options)));
	}

	public delete(url: string, options: RESTAdapterInterfaceOptions = {}) {
		if (this.auth.hasJWT() && !options.noAuth) {
			options.headers = this.getAuthHeaders();
		}

		return this.handle(this.http.delete(url, RESTAdapter.prepareRequestOptions(options)));
	}

	public getAuthHeaders(): RESTAdapterInterfaceHeader {
		let authHeader = {};

		if (this.auth.hasJWT()) {
			authHeader['Authorization'] = this.auth.getJWT();
		}

		return authHeader;
	}

	public handle(request) {
		let fork = request.publish().refCount();

		return fork.map(res => res.json());
	}

	private handleError(error) {
		let response = {
			success: false,
			status: 500,
			error: 'Unknown error'
		};

		if (typeof error === 'string') {
			this.genericError(error);
		} else if (typeof error === null) {
			this.unknownError();
		} else if (typeof error === 'object') {
			if (error instanceof Response) {
				try {
					let parsed = error.json();

					this.httpError(parsed.error);

					response.error = parsed.error;
					response.status = error.status;
				} catch (error) {
					this.jsonParseError();
				}
			} else {
				this.unknownError();
			}
		} else {
			this.unknownError();
		}

		return response;
	}

	private unknownError() {}
	private genericError(error: string) {}
	private jsonParseError() {}
	private httpError(error: string) {}
}

export function withStatusCode(code: number, error, callback: Function) {
	if (error instanceof Response) {
		if (error.status === code) {
			callback();
		}
	}
}
