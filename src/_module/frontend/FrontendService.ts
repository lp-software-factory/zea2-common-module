import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';

import {ZEA2Frontend} from '../../../../../definitions/src/zea2/src/definitions/frontend/entity/ZEA2Frontend';

@Injectable()
export abstract class FrontendService<T extends ZEA2Frontend>
{
    public replay: ReplaySubject<T> = new ReplaySubject<T>();

    abstract fetch(): Observable<T>;

    emit(next: T): void {
        this.replay.next(next);
    }
}
