import {RESTAdapter} from "./common/service/RESTAdapter";

import {AccountRESTService} from '../../../../definitions/src/zea2/src/services/account/AccountRESTService';
import {AttachmentRESTService} from '../../../../definitions/src/zea2/src/services/attachment/AttachmentRESTService';
import {AuthRESTService} from '../../../../definitions/src/zea2/src/services/auth/AuthRESTService';
import {LocaleRESTService} from '../../../../definitions/src/zea2/src/services/locale/LocaleRESTService';
import {OAuth2ProvidersRESTService} from '../../../../definitions/src/zea2/src/services/oauth2-providers/OAuth2ProvidersRESTService';
import {TranslationRESTService} from '../../../../definitions/src/zea2/src/services/translation/TranslationRESTService';

export function factoryAccountRESTService(adapter: RESTAdapter) {
    return new AccountRESTService(adapter);
}

export function factoryAttachmentRESTService(adapter: RESTAdapter) {
    return new AttachmentRESTService(adapter);
}

export function factoryAuthRESTService(adapter: RESTAdapter) {
    return new AuthRESTService(adapter);
}

export function factoryLocaleRESTService(adapter: RESTAdapter) {
    return new LocaleRESTService(adapter);
}

export function factoryOAuth2ProvidersRESTService(adapter: RESTAdapter) {
    return new OAuth2ProvidersRESTService(adapter);
}

export function factoryTranslationRESTService(adapter: RESTAdapter) {
    return new TranslationRESTService(adapter);
}