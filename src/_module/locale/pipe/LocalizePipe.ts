import {PipeTransform, Pipe} from "@angular/core";

import {LocaleService} from "../service/LocaleService";
import {LocalizedString} from "../../../../../../definitions/src/zea2/src/definitions/locale/definitions/LocalizedString";

@Pipe({
    name: 'localize'
})
export class LocalizePipe implements PipeTransform
{
    constructor(private localeService: LocaleService) {
    }

    transform(value: LocalizedString[], ...args: any[]): string {
        return this.localeService.getLocalization(value).value;
    }
}
